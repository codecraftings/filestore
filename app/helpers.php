<?php
function api_error($msg ="Bad Request", $code=400){
	//Log::error($msg);
	return Response::json(array(
		'status' => $code,
		'error'=>array('message'=>$msg)
		), $code);
}
function get_file_mime($file_path){
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$mime = finfo_file($finfo, $file_path);
	finfo_close($finfo);
	return $mime;
}
function flush_file($file_path){
	//dd(get_file_mime($file_path));
	$headers = array(
		'Cache-Control'             => 'max-age:3600',
		'Content-Type'              => get_file_mime($file_path),
		'Content-Transfer-Encoding' => 'binary',
		'Content-Disposition'       => 'inline',
		'Content-Length'			=> File::size($file_path),
		'ETag'						=> md5($file_path)
	);
	//dd($headers);
	$file = fopen($file_path,'r');
	return Response::stream(function() use ($file){
		fpassthru($file);
	}, 200, $headers);
}
function api_success($data, $code=200){
	$response = array(
		'status' => $code
		);
	if(is_string($data)){
		$response['message'] = $data;
	}
	elseif(is_array($data)){
		$response = array_merge($response, $data);
	}else{
		$response['data'] = $data;
	}
	return Response::json($response, $code);
}
function asset_url($path, $server){
	return Config::get('app.server_endpoint')[$server]."/".$path;
}