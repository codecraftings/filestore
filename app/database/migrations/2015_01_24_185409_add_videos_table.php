<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos',function($table){
			$table->bigIncrements('id');
			$table->integer('app_id');
			$table->integer('user_id');
			$table->char('server_id',20);
			$table->char('file_name',100);
			$table->text('file_path',300);
			$table->text('thumb_path',300);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('videos');
	}

}
