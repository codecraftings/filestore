<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auth', function($table){
			$table->increments('id');
			$table->integer('app_id');
			$table->text('token');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('expire_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('auth');
	}

}
