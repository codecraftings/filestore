<?php
use Carbon\Carbon;
class AuthController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return api_error();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return api_error();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$app_id = Input::get('app_id');
		$app_secret = Input::get('app_secret');
		if(!$app_id||!$app_secret){
			return api_error();
		}
		if($app_secret!=Config::get('app.SECRET_TOKEN')){
			return api_error('Invalid App Secret');
		}
		$token = Token::create(array(
			'app_id' => $app_id,
			'token' => Crypt::encrypt(str_random(10).'&'.$app_id.'&'."kkkkk"),
			'expire_at' => Carbon::now()->addHour()
			));
		if(!$token){
			return api_error('Error creating token');
		}
		return api_success($token->toArray());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
