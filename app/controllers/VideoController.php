<?php
use FS\Services\File\Uploader;
use FS\Services\File\VideoEditor;

class VideoController extends \BaseController {

	protected $uploader;
	protected $editor;

	public function __construct(Uploader $uploader, VideoEditor $editor){
		$this->uploader = $uploader;
		$this->editor = $editor;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function chunkUpload(){
		$chunk_num = Input::get('resumableChunkNumber');
		$chunk_size = Input::get('resumableChunkSize');
		$total_size = Input::get('resumableTotalSize');
		$total_chunks = Input::get('resumableTotalChunks');
		$file_id = Input::get('resumableIdentifier');
		$file_name = Input::get('resumableFilename');
		$chunk_name = $file_id."--".$chunk_num;
		if($chunk_num==7){
			//return api_error();
		}
		if($this->uploader->chunkExists($chunk_name)){
			if($this->uploader->countChunks($file_id)==$total_chunks){
				if(FALSE == $this->uploader->combineChunks($file_id, $file_name, $total_chunks, $total_size)){
					$this->uploader->clearChunks($file_id, $total_chunks);
					return api_error("Something strange happended! Try again later!");
				}
			}
			return api_success("Already Uploaded");
		}
		if(!Input::hasFile('filepart')||!$chunk_num){
			return api_error("No file sent!");
		}
		if($chunk_num==1){
			if(!$this->uploader->isValidVideo(Input::file('filepart'))){
				$this->uploader->clearChunks($file_id, $total_chunks);
				return api_error($this->uploader->errors()->first(), 501);
			}
		}
		if(FALSE == $this->uploader->saveChunk(Input::file('filepart'), $chunk_name)){
			return api_error("Unsuccessful", 202);
		}
		if($this->uploader->countChunks($file_id)==$total_chunks){
			if(FALSE == $this->uploader->combineChunks($file_id, $file_name, $total_chunks, $total_size)){
				$this->uploader->clearChunks($file_id, $total_chunks);
				return api_error("Something strange happended! Try again later!");
			}
		}
		return api_success("Upload Ok");
	}
	/**
	 * Store a newly created resource in storage.
	 * @param video file/s
	 * @param app_id
	 * @param access_token
	 * @param user_id
	 * @return Response
	 */
	public function store()
	{
		if(!Input::get('file_name')||!Input::get('user_id')){
			return api_error('No video file provided');
		}
		$file = $this->uploader->moveFromTempFolder(Input::get('file_name'));

		if(!$file){
			return api_error('Invalid file uploaded');
		}
		$file = $this->editor->convertToWebM($file);
		$thumb = $this->editor->generateThumb($file);
		$video = Video::create(array(
			'app_id' => Input::get('app_id'),
			'user_id' => Input::get('user_id'),
			'file_name' => $file->getFileName(),
			'file_path' => $file->getPublicPath(),
			'thumb_path' => $thumb->getPublicPath(),
			'server_id' => 'self'
			));
		if(!$video){
			return api_error('Something went wrong!');
		}		
		return api_success($video->toArray());
	}


	public function display($id, $type, $width, $height, $path){
		$video = Video::findOrFail($id);
		switch ($type) {
			case 'thumb':
				$path = $video->thumb_path;
				break;
			case 'original':
				$path = $video->file_path;
				break;
			default:
				# code...
				break;
		}
		//dd($path);
		if(file_exists($path)){
			//dd(get_file_mime($path));
			return flush_file($path);
		}
		if($type=="thumb"){
			return flush_file("images/no_image.jpg");
		}
		return api_error('Resource not found!', 404);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
