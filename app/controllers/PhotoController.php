<?php
use FS\Services\File\Uploader;
use FS\Services\File\ImageEditor;
class PhotoController extends \BaseController {

	protected $uploader;
	protected $editor;

	public function __construct(Uploader $uploader, ImageEditor $editor){
		$this->uploader = $uploader;
		$this->editor = $editor;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 * @param photo file/s
	 * @param app_id
	 * @param access_token
	 * @param user_id
	 * @return Response
	 */
	public function store()
	{
		if(!Input::hasFile('photo')||!Input::get('user_id')){
			return api_error();
		}
		$this->uploader->setFiles(Input::file('photo'));
		$this->uploader->acceptUploadImages();
		$files = $this->uploader->getMovedFiles();
		if(!is_array($files)){
			return api_error('Invalid file uploaded');
		}
		//dd($files);
		$maxW = 1000;
		$maxH = 1000;
		foreach ($files as $key => &$file) {
			if($maxW||$maxH){
				$this->editor->ensureMaxSize($file, $maxW, $maxH);
			}
			$thumb = $this->editor->generateThumb($file);
			$photo = Photo::create(array(
					'app_id' => Input::get('app_id'),
					'user_id' => Input::get('user_id'),
					'file_name' => $file->getFileName(),
					'file_path' => $file->getPublicPath(),
					'thumb_path' => $thumb->getPublicPath(),
					'server_id' => 'self',
				));
			if(!$photo){
				return api_error('Something went wrong!');
			}
			$file = $photo;
		}
		if(count($files)<1){
			return api_success($files);
		}else{
			return api_success($files[0]->toArray());
		}
	}

	public function display($id, $type, $width, $height, $path){
		//echo realpath($path);
		$photo = Photo::findOrFail($id);
		switch ($type) {
			case 'thumb':
				$path = $photo->thumb_path;
				break;
			case 'original':
				$path = $photo->file_path;
				break;
			default:
				$path = $photo->file_path;
				break;
		}
		if(file_exists($path)){
			return flush_file($path);
			//return Redirect::to(asset_url($path, $photo->server_id), 302);
		}
		return api_error('Resource not found!', 404);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$photo = Photo::findOrFail($id);
		return api_success($photo->toArray());
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
