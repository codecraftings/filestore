<?php
class Photo extends Eloquent{
	protected $table ="photos";
	protected $fillable = array('app_id','user_id','file_name','file_path','thumb_path','server_id');
}