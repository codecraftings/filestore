<?php
class Video extends Eloquent{
	protected $table ="videos";
	protected $fillable = array('app_id','user_id','file_name','file_path','thumb_path','server_id');
}