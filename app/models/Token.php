<?php
use Carbon\Carbon;
class Token extends Eloquent{
	protected $table ="auth";
	protected $fillable = array('app_id','token','expire_at');
	public static $current;
	public function expired(){
		if(Carbon::now()->diffInSeconds($this->expire_at, false)<0){
			return true;
		}
		return false;
	}
	public function getDates(){
		return ['created_at','updated_at','expire_at'];
	}
}