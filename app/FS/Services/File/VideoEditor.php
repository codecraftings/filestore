<?php
namespace FS\Services\File;
use Rafasamp\Sonus\Sonus;
class VideoEditor{
	protected $file;
	protected $image_manager;
	protected $manager;
	public function __construct(File $file,Sonus $manager){
		$this->file = $file;
		$this->manager = $manager;
	}
	public function generateThumb(File $video_file){
		$video_path = $video_file->getPath();
		$thumb_path = $video_file->getDir()->getPath().DIRECTORY_SEPARATOR.$video_file->getName()."-thumb-600x600.png";
		//$this->manager->getThumbnails($video_path, $thumb_path, 1);
		//$command = \Config::get('sonus::ffmpeg')." -ss 1 -i ".$video_path." -frames:v 1 ".$thumb_path;
		$command = "ffmpeg -ss 1 -i ".$video_path." -frames:v 1 ".$thumb_path;
		$c = shell_exec($command);
		\Log::info($c);
		return $this->file->fromPath($thumb_path);
	}
	public function convertToWebM(File $video_file){
		$video_path = $video_file->getPath();
		$webm_path = $video_file->getDir()->getPath().DIRECTORY_SEPARATOR.$video_file->getName().".webm";
		//$command = \Config::get('sonus::ffmpeg')." -i ".$video_path." ".$webm_path;
		$command = "ffmpeg -i ".$video_path." ".$webm_path;
		$c = shell_exec($command);
		\Log::info($c);
		return $this->file->fromPath($webm_path);
	}
	public function ensureMaxSize(File $img, $maxW, $maxH){
		return true;
	}
}