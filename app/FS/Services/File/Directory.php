<?php
namespace FS\Services\File;
use Illuminate\Filesystem\Filesystem;
class Directory{
	protected $path;
	protected $filesystem;
	public function __construct(Filesystem $files, $path=""){
		$this->filesystem = $files;
		$this->path = $path;
	}
	public function newInstance($path){
		return new self($this->filesystem, $path);
	}
	public function dirCount(){
		$dirs = $this->filesystem->directories($this->path);
		return count($dirs);
	}
	public function filesCount(){
		$files = $this->filesystem->files($this->path);
		return count($files);
	}
	public function addDir($name){
		$d = $this->path.DIRECTORY_SEPARATOR.$name;
		$this->filesystem->makeDirectory($d, 0755);
		return $this->newInstance($d);
	}
	public function lastDir(){
		$dirs = $this->filesystem->directories($this->path);
		sort($dirs, SORT_NATURAL|SORT_FLAG_CASE);
		if(count($dirs)<1){
			return false;
		}
		return $this->newInstance($dirs[count($dirs)-1]);
	}
	public function parent(){
		return $this->newInstance(dirname($this->path));
	}
	public function exists($dirOrFile){
		return $this->filesystem->exists($this->path.DIRECTORY_SEPARATOR.$dirOrFile);
	}
	public function gotoOrCreate($dir){
		if($this->exists($dir)){
			return $this->newInstance($this->path.DIRECTORY_SEPARATOR.$dir);
		}else{
			return $this->addDir($dir);
		}
	}
	public function getPath(){
		return $this->path;
	}
	public function dirName(){
		return basename($this->path);
	}
	public function fileName(){

	}
	public function uniqueFilename($extension, $title=""){
		$filename = uniqid()."-".str_replace(" ", "-", substr($title, 0,10)).".".$extension;
		if($this->filesystem->exists($this->path.DIRECTORY_SEPARATOR.$filename)){
			return $this->uniqueFilename($extension, $title);
		}
		return $filename;
	}

}