<?php
namespace FS\Services\File;
use Intervention\Image\ImageManager;
class ImageEditor{
	protected $file;
	protected $manager;
	public function __construct(File $file, ImageManager $manager){
		$this->file = $file;
		$this->manager = $manager;
	}
	public function generateThumb(File $img_file){
		$thumb = $this->manager->make($img_file->getPath());
		$thumb = $thumb->fit(300, 300, function ($constraint) {
		    $constraint->upsize();
		});
		$thumb_path = $img_file->getDir()->getPath().DIRECTORY_SEPARATOR.$img_file->getName()."-thumb-300x300.png";
		$thumb->save($thumb_path, 60);
		$thumb = $this->file->fromPath($thumb_path);
		return $thumb;
	}
	public function ensureMaxSize(File $img, $maxW, $maxH){
		$img = $this->manager->make($img->getPath());
		$img = $img->resize($maxW, $maxH, function ($constraint) {
		    //$constraint->aspectRatio();
		    $constraint->upsize();
		})->save();
		return true;
	}
}