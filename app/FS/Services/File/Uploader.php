<?php
namespace FS\Services\File;
use FS\Services\Validator\ImageValidator;
use Illuminate\Support\MessageBag;
use FS\Services\File\Directory;
use FS\Services\File\File;
use FS\Services\Validator\VideoValidator;
class Uploader{
	protected $upload_path;
	protected $video_upload_path;
	protected $temp_path;
	public $moved_files;
	protected $uploading_files;
	protected $image_validator;
	protected $errors;
	protected $directory;
	protected $file;
	protected $video_validator;

	public function __construct(ImageValidator $image_validator, VideoValidator $video_validator, MessageBag $bag, Directory $dir, File $file){
		$this->upload_path = public_path().DIRECTORY_SEPARATOR.'uploads';
		$this->video_upload_path = public_path().DIRECTORY_SEPARATOR.'videos';
		$this->temp_path = base_path().DIRECTORY_SEPARATOR.'temp';
		$this->image_validator = $image_validator;
		$this->errors = $bag;
		$this->directory = $dir;
		$this->file = $file;
		$this->video_validator = $video_validator;
	}
	public function chunkExists($chunk_name){
		return file_exists($this->temp_path.DIRECTORY_SEPARATOR.$chunk_name);
	}
	public function isValidVideo($filepart){
		\Log::error($filepart->getMimeType());
		$valid_mimes = array("video/mp4","video/webm","video/x-ms-wmv","video/x-ms-asf");
		$this->video_validator->with(array('file'=>$filepart));
		if($this->video_validator->passes()){
			$filemime = $filepart->getMimeType();
		 	if(in_array($filemime, $valid_mimes)){
		 		return true;
		 	}else{
		 		$this->errors->add("format", $filemime." format not supported!");
		 		return false;
		 	}
		}
		$this->errors = $this->video_validator->errors();
		return false;
	}
	public function saveChunk($filepart, $chunk_name){
		return $filepart->move($this->temp_path, $chunk_name);
	}
	public function countChunks($file_id){
		$chunks = glob($this->temp_path.DIRECTORY_SEPARATOR.$file_id."--*");
		return count($chunks);
	}
	public function combineChunks($file_id, $file_name, $total_chunks, $total_size){
		$file_path = $this->temp_path.DIRECTORY_SEPARATOR.$file_id.$file_name;
		/*$chunks = glob($this->temp_path.DIRECTORY_SEPARATOR.$file_id."--*");
		if(count($chunks)<$total_chunks){
			return false;
		}*/
		$file = fopen($file_path, "wb");
		if(!$file){
			return false;
		}
		for($i=1;$i<=$total_chunks;$i++){
			$chunk_path = $this->temp_path.DIRECTORY_SEPARATOR.$file_id."--".$i;
			$chunk = fopen($chunk_path, 'rb');
			if(!$chunk){
				return false;
			}
			while (!feof($chunk)) {
				if(!fwrite($file, fread($chunk, 1024))){
					continue;
				}
			}
			fclose($chunk);
			unlink($chunk_path);
		}
		fclose($file);
		$file = $this->file->fromPath($file_path);
		return $file;
	}
	public function clearChunks($file_id, $total_chunks){
		for($i=1;$i<=$total_chunks;$i++){
			$chunk_path = $this->temp_path.DIRECTORY_SEPARATOR.$file_id."--".$i;
			if(file_exists($chunk_path))
				unlink($chunk_path);
		}
	}
	public function moveFromTempFolder($file_name){
		$file = $this->file->fromPath($this->temp_path.DIRECTORY_SEPARATOR.$file_name);
		$move_dir = $this->getCurrentUploadDir($this->video_upload_path);
		$newName = $move_dir->uniqueFilename($file->getExtension(), $file->getName());
		$move_path = $move_dir->getPath().DIRECTORY_SEPARATOR.$newName;

		if(rename(realpath($file->getPath()), $move_path)){
			$file = $this->file->fromPath($move_path);
			return $file;
		}
	}
	public function setFiles($files){
		if(is_array($files)){
			$this->uploading_files = array();
			foreach ($files as $key => $file) {
				array_push($this->uploading_files, $this->file->fromUploadedFile($file));
			}
		}
		else{
			$this->uploading_files = array($this->file->fromUploadedFile($files));
		}
	}
	public function acceptUploadImages(){
		$this->moved_files = array();
		//dd($this->uploading_files);
		foreach ($this->uploading_files as $key => $file) {
			$this->image_validator->with(array('file'=>$file->uploaded_file));
			if($this->image_validator->passes()){
				$move_dir = $this->getCurrentUploadDir($this->upload_path);
				$ext = $file->getExtension();
				$newName = $move_dir->uniqueFilename($ext, $file->getName());
				$file->uploaded_file->move($move_dir->getPath(), $newName);
				$file->filename = $newName;
				$file->path = $move_dir->getPath().DIRECTORY_SEPARATOR.$newName;
				$file->uploaded_file = null;
				array_push($this->moved_files, $file);
			}else{
				continue;
			}
		}
	}
	public function acceptUploadVideos(){
		$this->moved_files = array();
		//dd($this->uploading_files);
		foreach ($this->uploading_files as $key => $file) {
			$this->video_validator->with(array('file'=>$file->uploaded_file));
			if($this->video_validator->passes()){
				$move_dir = $this->getCurrentUploadDir($this->video_upload_path);
				$ext = $file->getExtension();
				$newName = $move_dir->uniqueFilename($ext, $file->getName());
				$file->uploaded_file->move($move_dir->getPath(), $newName);
				$file->filename = $newName;
				$file->path = $move_dir->getPath().DIRECTORY_SEPARATOR.$newName;
				$file->uploaded_file = null;
				array_push($this->moved_files, $file);
			}else{
				continue;
			}
		}
	}
	public function getMovedFiles(){
		if(empty($this->moved_files)||count($this->moved_files)<1){
			$this->errors->add("Uploader", "No File Uploaded");
			return false;
		}
		return $this->moved_files;
	}
	public function getCurrentUploadDir($base_path){
		$dir = $this->directory->newInstance($base_path);
		$dir = $dir->gotoOrCreate(date('Y'))->gotoOrCreate(date('n'));
		if($dir->dirCount()<1){
			$dir = $dir->addDir('1');
		}else{
			$dir = $dir->lastDir();
		}
		if($dir->filesCount()>100){
			$newName = $dir->dirName()+1;
			$dir = $dir->parent()->addDir($newName);
		}
		return $dir;
	}
	public function errors(){
		return $this->errors;
	}
}