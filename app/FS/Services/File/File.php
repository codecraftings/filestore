<?php
namespace FS\Services\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use FS\Services\File\Directory;
class File{
	public $filename;
	protected $name;
	protected $ext;
	public $path;
	public $size;
	protected $url;
	protected $dir;
	public $uploaded_file;
	public function __construct(Directory $dir){
		$this->dir = $dir;
	}
	public function newInstance(){
		return new self($this->dir);
	}
	public function fromPath($path){
		$a = new self($this->dir);
		$a->path = $path;
		$a->filename = basename($path);
		return $a;
	}
	public function fromUploadedFile($uploaded_file){
		$a = new self($this->dir);
		$a->uploaded_file = $uploaded_file;
		$a->filename = $uploaded_file->getClientOriginalName();
		return $a;
	}
	public function getDir(){
		return $this->dir->newInstance(dirname($this->path));
	}
	public function getExtension(){
		if(!$this->ext){
			$e = explode(".", $this->filename);
			$this->ext = $e[count($e)-1];
			$this->name = $e[0];
		}
		return $this->ext;
	}
	public function getFileName(){
		return $this->filename;
	}
	public function getName(){
		$e = explode(".", $this->filename);
		$this->ext = $e[count($e)-1];
		$this->name = $e[0];
		return $this->name;
	}
	public function getUrl(){
		if(!$this->url){
			$tmp = substr($this->path, strlen(public_path())+1);
			$tmp = str_replace("\\", "/", $tmp);
			$this->url = url($tmp);
		}
		return $this->url;
	}
	public function getPath(){
		return $this->path;
	}
	public function getPublicPath(){
		if(!str_contains($this->path, public_path())){
			return false;
		}
		$tmp = substr($this->path, strlen(public_path())+1);
		$tmp = str_replace("\\", "/", $tmp);
		return $tmp;
	}
}