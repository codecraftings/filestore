<?php
namespace FS\Services\Validator;
class CaptchaValidator extends LaravelValidator{
	protected $rules = array(
			'recaptcha_response_field' => 'required|recaptcha'
		);
}