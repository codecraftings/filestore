<?php
namespace FS\Services\Validator;
interface ValidatorInterface{
	public function with(array $data);
	public function passes();
	public function fails();
	public function errors();
}