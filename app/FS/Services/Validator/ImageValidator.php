<?php
namespace FS\Services\Validator;
class ImageValidator extends LaravelValidator{
	protected $rules = array(
			'file' => 'required|image|max:2048'
		);
}