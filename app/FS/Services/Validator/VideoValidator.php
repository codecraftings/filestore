<?php
namespace FS\Services\Validator;
use FS\Services\Validator\LaravelValidator;
class VideoValidator extends LaravelValidator{
	protected $rules = array(
			//"file" => "required|mimes:video/mp4,video/webm,video/x-ms-wmv,video/x-ms-asf,image/jpeg|max:500000"
			//"file" => "required|mimes:mp4,webm,wmv,jpg|max:500000"
			"file" => "required|max:400000"
		);
}