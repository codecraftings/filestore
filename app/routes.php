<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
$monolog = Log::getMonolog();
$syslog = new \Monolog\Handler\SyslogHandler('papertrail');
$formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');
$syslog->setFormatter($formatter);

$monolog->pushHandler($syslog);

Route::get('/', function()
{
	echo "testtttt";
});
Route::pattern('path','[a-zA-Z0-9_\-\.\/]+');
Route::pattern('id','[0-9]+');
Route::pattern('width','[0-9]+');
Route::pattern('height','[0-9]+');
Route::get('resource/image/{id}/{type}/{width}x{height}/{path}', 'PhotoController@display');
Route::get('resource/video/{id}/{type}/{width}x{height}/{path}', 'VideoController@display');

Route::resource('auth','AuthController');

Route::group(array('before'=>'verify_token'), function(){
	Route::resource('photos','PhotoController');
	Route::get('test',function(){
		return "ff";
	});
	Route::get('videoupload','VideoController@chunkUpload');
	Route::post('videoupload','VideoController@chunkUpload');
	Route::resource('video','VideoController');
});